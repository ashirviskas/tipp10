/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
*/

/****************************************************************
**
** Implementation of the ErrorMessage class
** File name: errormessage.cpp
**
****************************************************************/

#include <QMessageBox>

#include "def/defines.h"
#include "def/errordefines.h"
#include "errormessage.h"

ErrorMessage::ErrorMessage(QWidget* parent)
    : QWidget(parent)
{
}

void ErrorMessage::showMessage(
    int errorNo, Type errorType, Cancel cancelProcedure, QString addon)
{
    QString message;
    // First generate the error text
    message = getErrorText(errorNo);
    // If exist, append an additional text
    if (addon != "") {
        message.append("\n" + addon);
    }
    /*if (cancelProcedure != ErrorMessage::Cancel::No) {
            // Append fix text
            message.append(tr("\n\nBitte melden Sie den Fehler "
                    "(Fehlernummer) und die Umstaende unter denen er aufgetreten
    " "ist im Internet unter: ") + APP_URL
                    + tr("\nSo kann ") + APP_NAME
                    + tr(" verbessert werden. Danke!"));
    }*/

    // Append, what the program do now
    message.append("\n\n" + getCancelText(cancelProcedure));

    // Choose a message style
    switch (errorType) {
    case Type::Info:
        QMessageBox::information(nullptr, APP_NAME, message);
        break;
    case Type::Warning:
        QMessageBox::warning(nullptr, APP_NAME, message);
        break;
    case Type::Critical:
        QMessageBox::critical(nullptr, APP_NAME, message);
        break;
    }
}

QString ErrorMessage::getCancelText(Cancel type)
{
    QString cancelText = "";
    switch (type) {
    case Cancel::No:
        cancelText = "";
        break;
    case Cancel::Operation:
        cancelText = tr("The process will be aborted.");
        break;
    case Cancel::Update:
        cancelText = tr("The update will be aborted.");
        break;
    case Cancel::Program:
        cancelText = tr("The program will be aborted.");
        break;
    }
    return cancelText;
}

QString ErrorMessage::getErrorText(int number)
{
    QString errorText = "";
    switch (number) {
    case ERR_LOGO_PIC:
        errorText = tr("Cannot load the program logo.");
        break;
    case ERR_KEY_PIC:
        errorText = tr("Cannot load the keyboard bitmap.");
        break;
    case ERR_TICKER_PIC:
        errorText = tr("Cannot load the timer background.");
        break;
    case ERR_STATUS_PIC:
        errorText = tr("Cannot load the status bar background.");
        break;

    case ERR_SQL_DB_APP_EXIST:
        errorText
            = tr("Cannot find the database %1. The file could not be "
                 "imported.\nPlease check whether it is a readable text file.")
                  .arg(APP_DB);
        break;
    case ERR_SQL_DB_USER_EXIST:
        errorText = tr("Cannot find the database in the specified "
                       "directory.\nTIPP10 is trying to create a new, empty "
                       "database in the specified directory.\n\nYou can change "
                       "the path to the database in the program settings.\n");
        break;
    case ERR_SQL_DB_APP_COPY:
        errorText = tr(
            "Cannot create the user database in your HOME directory. There may "
            "be no permission to write.\nTIPP10 is trying to use the original "
            "database in the program directory.\n\nYou can change the path to "
            "the database in the program settings later.\n");
        break;
    case ERR_SQL_DB_USER_COPY:
        errorText = tr("Cannot create the user database in the specified "
                       "directory. There may be no directory or no permission "
                       "to write.\nTIPP10 is trying to create a new, empty "
                       "database in your HOME directory.\n\nYou can change the "
                       "path to the database in the program settings later.\n");
        break;
    case ERR_SQL_CONNECTION:
        errorText = tr("Connection to the database failed.");
        break;

    case ERR_DB_VERSION_EXIST:
        errorText = tr("Connection to the database failed.");
        break;
    case ERR_USER_LESSONS_FLUSH:
        errorText = tr("The user table with lesson data  cannot be "
                       "emptied.\nSQL statement failed.");
        break;
    case ERR_USER_ERRORS_FLUSH:
        errorText = tr("The user table with error data cannot be emptied.\nSQL "
                       "statement failed.");
        break;
    case ERR_LESSONS_EXIST:
        errorText = tr("No lessons exist.");
        break;
    case ERR_LESSONS_SELECTED:
        errorText = tr("No lesson selected.\nPlease select a lesson.");
        break;
    case ERR_LESSONS_CREATION:
        errorText = tr("Cannot create the lesson.\nSQL statement failed.");
        break;
    case ERR_LESSONS_UPDATE:
        errorText
            = tr("The lesson could not be updated because there is no\naccess "
                 "to the database.\n\nIf this problem only occurred after the "
                 "software had been\nrunning smoothly for some time, the "
                 "database is expected\nto have been damaged (eg crashing of "
                 "the computer).\nTo check whether or not the database has "
                 "been damaged,\nyou can rename the database file and restart "
                 "the software (it\nwill create a new, empty database "
                 "automatically).\nYou can find the database path \"%1\" in "
                 "the General Settings.\n\nIf this problem occurred after the "
                 "first time the software was\nstarted, please check the write "
                 "privileges on the database\nfile.")
                  .arg(APP_USER_DB);
        break;
    case ERR_USER_ERRORS_REFRESH:
        errorText
            = tr("The user typing errors could not be updated because\nthere "
                 "is no access to the database.\n\nIf this problem only "
                 "occurred after the software had been\nrunning smoothly for "
                 "some time, the database is expected\nto have been damaged "
                 "(eg crashing of the computer).\nTo check whether or not the "
                 "database has been damaged,\nyou can rename the database file "
                 "and restart the software (it\nwill create a new, empty "
                 "database automatically).\nYou can find the database path "
                 "\"%1\" in the General Settings.\n\nIf this problem occurred "
                 "after the first time the software was\nstarted, please check "
                 "the write privileges on the database\nfile.")
                  .arg(APP_USER_DB);
        break;
    case ERR_USER_LESSONS_REFRESH:
        errorText
            = tr("The user lesson data could not be updated because\nthere is "
                 "no access to the database.\n\nIf this problem only occurred "
                 "after the software had been\nrunning smoothly for some time, "
                 "the database is expected\nto have been damaged (eg crashing "
                 "of the computer).\nTo check whether or not the database has "
                 "been damaged,\nyou can rename the database file and restart "
                 "the software (it\nwill create a new, empty database "
                 "automatically).\nYou can find the database path \"%1\" in "
                 "the General Settings.\n\nIf this problem occurred after the "
                 "first time the software was\nstarted, please check the write "
                 "privileges on the database\nfile.")
                  .arg(APP_USER_DB);
        break;
    case ERR_USER_LESSON_ADD:
        errorText = tr("Cannot save the lesson.\nSQL statement failed.");
        break;
    case ERR_USER_LESSON_GET:
        errorText = tr("Cannot retrieve the lesson.\nSQL statement failed.");
        break;
    case ERR_USER_LESSON_ANALYZE:
        errorText = tr("Cannot analyze the lesson.\nSQL statement failed.");
        break;
    case ERR_USER_IMPORT_READ:
        errorText = tr("The file could not be imported.\nPlease check whether "
                       "it is a readable text file.\n");
        break;
    case ERR_USER_IMPORT_EMPTY:
        errorText
            = tr("The file could not be imported because it is empty.\nPlease "
                 "check whether it is a readable text file with content.\n");
        break;
    case ERR_USER_DOWNLOAD_EXECUTION:
        errorText
            = tr("The file could not be imported.\nPlease check the spelling "
                 "of the web address;\nit must be a valid URL and a readable "
                 "text file.\nPlease also check your internet connection.");
        break;
    case ERR_USER_EXPORT_WRITE:
        errorText = tr("The file could not be exported.\nPlease check to see "
                       "whether it is a writable text file.\n");
        break;
    case ERR_TEMP_FILE_CREATION:
        errorText = tr("Cannot create temporary file.");
        break;
    case ERR_UPDATE_EXECUTION:
        errorText = tr("Cannot execute the update process.\nPlease check your "
                       "internet connection and proxy settings.");
        break;
    case ERR_ONLINE_VERSION_READABLE:
        errorText = tr("Cannot read the online update version.");
        break;
    case ERR_DB_VERSION_READABLE:
        errorText = tr("Cannot read the database update version.");
        break;
    case ERR_UPDATE_SQL_EXECUTION:
        errorText = tr("Cannot execute the SQL statement.");
        break;
    case ERR_ERROR_DEFINES_EXIST:
        errorText = tr("Cannot find typing mistake definitions.");
        break;
    case ERR_LESSON_CONTENT_EXIST:
        errorText = tr("Cannot create temporary file.\nUpdate failed.");
        break;
    case ERR_ANALYZE_TABLE_CREATION:
        errorText = tr("Cannot create analysis table.");
        break;
    case ERR_ANALYZE_INDEX_CREATION:
        errorText = tr("Cannot create analysis index.");
        break;
    case ERR_ANALYZE_TABLE_FILL:
        errorText = tr("Cannot fill analysis table with values.");
        break;

    default:
        errorText = tr("An error has occured.");
        break;
    }
    // Append the error number
    errorText.append(tr("\n(Error number: %1)\n").arg(QString::number(number)));
    return errorText;
}
